FROM golang:alpine AS build

RUN apk add --no-cache git build-base ca-certificates yarn openjdk11-jre-headless
RUN yarn global add @openapitools/openapi-generator-cli
RUN go get entgo.io/ent/cmd/ent
RUN go get github.com/markbates/pkger/cmd/pkger
RUN go get github.com/swaggo/swag/cmd/swag

ADD go.mod /build/
WORKDIR /build/
RUN go mod download -x
ADD . /build/
RUN go generate -x && yarn build
RUN CGO_ENABLED=1 go build -v -ldflags '-s -w -extldflags "-static"' -o /tmp/server ./cmd/*

FROM scratch
COPY --from=build /etc/ssl /etc/nsswitch.conf /etc/
COPY --from=build /tmp/server /bin/server
COPY --from=build /build/ui/dist /app/ui/dist
WORKDIR /app
EXPOSE 80
ENTRYPOINT ["/bin/server"]
