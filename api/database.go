package api

import (
	"codeberg.org/wuks/reservator/ent"
	"context"
	"database/sql"
	"github.com/rs/zerolog/log"
)

var db *ent.Client
var dbRaw *sql.DB

func SetupDatabase(driverName string, dataSourceName string) error {
	var err error

	if db != nil {
		log.Info().Str("driver", driverName).Str("dataSource", dataSourceName).Msg("Disconnecting from database.")
		if err = db.Close(); err != nil {
			return err
		}
	}

	log.Info().Str("driver", driverName).Str("dataSource", dataSourceName).Msg("Connecting to database.")
	if dbRaw, err = sql.Open(driverName, dataSourceName); err != nil {
		return err
	}
	if db, err = ent.Open(driverName, dataSourceName); err != nil {
		return err
	}

	return db.Schema.Create(context.Background())
}
