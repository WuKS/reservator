// @title Reservator
// @version 1.0
// @description Reservator ist ein Buchungskalender für die Werk- und Kulturscheune Loitsche.

// @contact.name Werk- und Kulturscheune Loitsche e. V.
// @contact.url https://werkundkultur.de
// @contact.email admin@werkundkultur.de

// @license.name MIT License
// @license.url https://spdx.org/licenses/MIT.html

// @host belegung.werkundkultur.de
// @host localhost:8080
// @schemes http https
// @BasePath /api

package api

import (
	_ "codeberg.org/wuks/reservator/docs"
	"github.com/swaggo/files"
	"github.com/swaggo/gin-swagger"
)

var swagger = ginSwagger.WrapHandler(swaggerFiles.Handler, ginSwagger.URL("/api/swagger/doc.json"))
