package api

import (
	"codeberg.org/wuks/reservator/ent"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

// assureSuccess handles an Ent error, conditionally returning a StatusNotFound, StatusBadRequest or InternalServerError.
// It returns the value of !c.IsAborted(), so it can be used easily to check if the request was successful.
func assureSuccess(c *gin.Context, err error) bool {
	if err != nil && ent.IsNotFound(err) {
		c.AbortWithError(http.StatusNotFound, err)
	} else if err != nil && ent.IsValidationError(err) {
		c.AbortWithError(http.StatusBadRequest, err)
	} else if err != nil && (strings.Contains(strings.ToLower(err.Error()), "foreign key constraint fail") || strings.Contains(strings.ToLower(err.Error()), "violates foreign key constraint")) {
		c.AbortWithError(http.StatusBadRequest, err)
	} else if err != nil && ent.IsConstraintError(err) {
		c.AbortWithError(http.StatusConflict, err)
	} else if err != nil {
		panic(err)
	}
	return !c.IsAborted()
}

// assureNotAborted checks if c.IsAborted(), and if that's the case, panics with the nil value.
func assureNotAborted(c *gin.Context) {
	if c.IsAborted() {
		panic(nil)
	}
}
