package api

import (
	"codeberg.org/momar/webapp-boilerplate/builders"
	"codeberg.org/momar/webapp-boilerplate/request"
	"codeberg.org/wuks/reservator/ent"
	"codeberg.org/wuks/reservator/ent/reservation"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"net/http"
	"time"
)

// reservationList returns a list of reservations in a specified date range
// @Summary List reservations
// @Description List all accessible reservations in a specified date range
// @ID listReservations
// @Tags Reservation
// @Param start query string false "start date, using the format YYYY-MM-DD"
// @Param days query int false "number of days to query"
// @Produce json
// @Success 200 {object} []ent.Reservation "OK"
// @Router /reservation [get]
func reservationList(c *gin.Context) {
	start := request.From(c).Query("start").
		Default(time.Now().Add(-30 * 24 * time.Hour).Format("2006-01-02")).AsTime("2006-01-02").Get()
	days := request.From(c).Query("days").
		Default("90").AsNumber().Min(1).Max(365).Get()
	assureNotAborted(c)

	reservations, err := db.Reservation.
		Query().
		Order(ent.Asc(reservation.FieldBegin)).
		Where(
			reservation.EndGTE(start),
			reservation.BeginLTE(start.Add(time.Duration(days)*24*time.Hour)),
		).
		WithSubjects().
		All(c)
	if assureSuccess(c, err) {
		if isAdmin(c) {
			c.JSON(http.StatusOK, reservations)
		}else{
			c.JSON(http.StatusOK, anonymizeReservations(reservations))
		}
	}
}

// reservationCreate creates a new reservation in the database
// @Summary Create reservation
// @Description Create or request a new reservation
// @ID createReservation
// @Tags Reservation
// @Accept json
// @Param reservation body ent.Reservation true "Reservation to create or request"
// @Produces plain
// @Success 201 "Created"
// @Failure 400 "Bad Request"
// @Router /reservation [post]
func reservationCreate(c *gin.Context) {
	// Populate creation builder from body
	create := db.Reservation.Create()
	builders.ApplyContext(c, &ent.Reservation{}, create)
	assureNotAborted(c)

	// Save new reservation
	id := uuid.New()
	if _, err := create.SetID(id).Save(c); assureSuccess(c, err) {
		c.Redirect(http.StatusCreated, "reservation/" + id.String())
	}
}

// reservationRetrieve retrieves a reservation by UUID from the database
// @Summary Retrieve a reservation
// @Description Retrieve a single reservation by its UUID
// @ID retrieveReservation
// @Tags Reservation
// @Param id path string true "Reservation UUID"
// @Produces json
// @Success 200 {object} ent.Reservation "OK"
// @Failure 400 "Bad Request"
// @Router /reservation/{id} [get]
func reservationRetrieve(c *gin.Context) {
	id := request.From(c).Param("id").Required().AsUUID().Get()
	assureNotAborted(c)

	reservation, err := db.Reservation.
		Query().
		Where(reservation.ID(id)).
		WithSubjects().
		Only(c)
	if assureSuccess(c, err) {
		if isAdmin(c) {
			c.JSON(http.StatusOK, reservation)
			return
		}
		r, showNot := anonymizeReservation(*reservation)
		if showNot {
			c.Status(http.StatusForbidden)
			return
		}
		c.JSON(http.StatusOK, r)
	}
}

// reservationUpdate updates a reservation by UUID in the database
// @Summary Update a reservation
// @Description Update a single reservation by its UUID
// @ID updateReservation
// @Tags Reservation
// @Param id path string true "Reservation UUID"
// @Param reservation body ent.Reservation true "Updated reservation"
// @Produces plain
// @Success 200 "OK"
// @Failure 400 "Bad Request"
// @Router /reservation/{id} [put]
func reservationUpdate(c *gin.Context) {
	if !isAdmin(c) {
		c.AbortWithStatus(http.StatusForbidden)
		return
	}
	id := request.From(c).Param("id").Required().AsUUID().Get()
	assureNotAborted(c)

	update := db.Reservation.UpdateOneID(id)
	builders.ApplyContext(c, &ent.Reservation{}, update)
	assureNotAborted(c)

	if _, err := update.Save(c); assureSuccess(c, err) {
		c.Status(http.StatusOK)
	}
}

// reservationUpdate deletes a reservation by UUID in the database
// @Summary Delete a reservation
// @Description Delete a single reservation by its UUID
// @ID deleteReservation
// @Tags Reservation
// @Param id path string true "Reservation UUID"
// @Produces plain
// @Success 200 "OK"
// @Failure 400 "Bad Request"
// @Router /reservation/{id} [delete]
func reservationDelete(c *gin.Context) {
	if !isAdmin(c) {
		c.AbortWithStatus(http.StatusForbidden)
		return
	}
	id := request.From(c).Param("id").Required().AsUUID().Get()
	assureNotAborted(c)

	if err := db.Reservation.DeleteOneID(id).Exec(c); assureSuccess(c, err) {
		c.Status(http.StatusOK)
	}
}
