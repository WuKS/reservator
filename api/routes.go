package api

import (
	"github.com/gin-gonic/gin"
)

func SetupRoutes(router gin.IRouter, prefix string) {
	router.GET(prefix + "/swagger/*any", swagger)

	router.GET(prefix + "/reservation", reservationList)
	router.POST(prefix + "/reservation", reservationCreate)
	router.GET(prefix + "/reservation/:id", reservationRetrieve)
	router.PUT(prefix + "/reservation/:id", reservationUpdate)
	router.DELETE(prefix + "/reservation/:id", reservationDelete)

	router.GET(prefix + "/subject", subjectList)
	router.POST(prefix + "/subject", subjectCreate)
	router.GET(prefix + "/subject/:id", subjectRetrieve)
	router.PUT(prefix + "/subject/:id", subjectUpdate)
	router.DELETE(prefix + "/subject/:id", subjectDelete)
}
