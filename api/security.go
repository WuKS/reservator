package api

import (
	"codeberg.org/momar/webapp-boilerplate/env"
	"codeberg.org/wuks/reservator/ent"
	"codeberg.org/wuks/reservator/ent/reservation"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"strings"
)


var token =  env.Read("TOKEN", true).AsString().Get()

// tests if context contains admin token
// if admin token is valid, return true
func isAdmin(c *gin.Context) bool {
	auth := c.GetHeader("Authorization")
	if strings.TrimSpace(strings.TrimPrefix(auth, "Bearer")) == token {
		return true
	}
	return false
}

func anonymizeReservations(reservations []*ent.Reservation) []ent.Reservation {
	result := []ent.Reservation{}
	for _, r := range reservations {
		if r, notShow := anonymizeReservation(*r); !notShow {
			result = append(result, r)
		}
	}
	return result
}

func anonymizeReservation(r ent.Reservation) (ent.Reservation, bool) {
	log.Debug().Str("ID", r.ID.String()).Str("status", r.Status.String()).Msg("Anonymizing")
	if r.Status == reservation.StatusRequest || r.Status == reservation.StatusDeclined {
		log.Debug().Msg("Not added")
		return r, true
	}
	if r.Status == reservation.StatusPublic {
		return r, false
	}
	r.Title = "private Veranstaltung"
	r.Contact = ""
	r.Description = ""
	r.Email = ""
	return r, false
}