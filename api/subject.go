package api

import (
	"codeberg.org/momar/webapp-boilerplate/builders"
	"codeberg.org/momar/webapp-boilerplate/request"
	"codeberg.org/wuks/reservator/ent"
	"codeberg.org/wuks/reservator/ent/subject"
	"github.com/gin-gonic/gin"
	"net/http"
)

// subjectList returns a list of all subjects
// @Summary List subjects
// @Description List all configured subjects
// @ID listSubjects
// @Tags Subject
// @Produce json
// @Success 200 {object} []ent.Subject "OK"
// @Router /subject [get]
func subjectList(c *gin.Context) {
	if subjects, err := db.Subject.Query().All(c); assureSuccess(c, err) {
		c.JSON(http.StatusOK, subjects)
	}
}

// subjectCreate creates a new subject in the database
// @Summary Create subject
// @Description Create a new subject
// @ID createSubject
// @Tags Subject
// @Accept json
// @Param authorization header string true "Header for bearer authentication"
// @Param subject body ent.Subject true "Subject to create or request"
// @Produces plain
// @Success 201 "Created"
// @Failure 400 "Bad Request"
// @Failure 409 "Conflict"
// @Router /subject [post]
func subjectCreate(c *gin.Context) {
	if !isAdmin(c) {
		c.AbortWithStatus(http.StatusForbidden)
		return
	}
	// Populate creation builder from body
	create := db.Subject.Create()
	builders.ApplyContext(c, &ent.Subject{}, create)
	assureNotAborted(c)

	// Save new subject
	if subject, err := create.Save(c); assureSuccess(c, err) {
		c.Redirect(http.StatusCreated, "subject/" + subject.ID)
	}
}

// subjectRetrieve retrieves a subject by name from the database
// @Summary Retrieve a subject
// @Description Retrieve a single subject by its name
// @ID retrieveSubject
// @Tags Subject
// @Param id path string true "Subject name"
// @Produces json
// @Success 200 {object} ent.Subject "OK"
// @Failure 400 "Bad Request"
// @Failure 404 "Not Found"
// @Router /subject/{id} [get]
func subjectRetrieve(c *gin.Context) {
	id := request.From(c).Param("id").Required().AsString().MinLen(1).Get()
	assureNotAborted(c)

	if reservation, err := db.Subject.Get(c, id); assureSuccess(c, err) {
		c.JSON(http.StatusOK, reservation)
	}
}

// subjectUpdate updates a subject by name in the database
// @Summary Update a subject
// @Description Update a single subject by its name
// @ID updateSubject
// @Tags Subject
// @Param authorization header string true "Header for bearer authentication"
// @Param id path string true "Subject name"
// @Param subject body ent.Subject true "Updated subject"
// @Produces plain
// @Success 200 "OK"
// @Failure 400 "Bad Request"
// @Failure 409 "Conflict"
// @Failure 404 "Not Found"
// @Router /subject/{id} [put]
func subjectUpdate(c *gin.Context) {
	if !isAdmin(c) {
		c.AbortWithStatus(http.StatusForbidden)
		return
	}
	id := request.From(c).Param("id").Required().AsString().MinLen(1).Get()
	assureNotAborted(c)

	update := db.Subject.Update().Where(subject.ID(id))
	newSubject := &ent.Subject{}
	builders.ApplyContext(c, newSubject, update)
	assureNotAborted(c)

	if _, err := update.Save(c); assureSuccess(c, err) {
		if newSubject.ID != id {
			// TODO: in Ent, ID fields are always immutable for some reason - we should figure out how to fix that at some point.
			_, err := dbRaw.Exec("UPDATE " + subject.Table + " SET " + subject.FieldID + " = ? WHERE " + subject.FieldID + " = ?", newSubject.ID, id)
			if !assureSuccess(c, err) {
				return
			}
		}

		c.Status(http.StatusOK)
	}
}

// subjectDelete deletes a subject by name in the database
// @Summary Delete a subject
// @Description Delete a single subject by its name
// @ID deleteSubject
// @Tags Subject
// @Param authorization header string true "Header for bearer authentication"
// @Param id path string true "Subject name"
// @Produces plain
// @Success 200 "OK"
// @Failure 400 "Bad Request"
// @Failure 404 "Not Found"
// @Router /subject/{id} [delete]
func subjectDelete(c *gin.Context) {
	if !isAdmin(c) {
		c.AbortWithStatus(http.StatusForbidden)
		return
	}
	id := request.From(c).Param("id").Required().AsString().MinLen(1).Get()
	assureNotAborted(c)

	if err := db.Subject.DeleteOneID(id).Exec(c); assureSuccess(c, err) {
		c.Status(http.StatusOK)
	}
}
