package api

import (
	"bytes"
	boilerplate "codeberg.org/momar/webapp-boilerplate"
	"codeberg.org/wuks/reservator/ent"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/magiconair/properties/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestSubjectCreateWithoutPermission(t *testing.T) {
	gin.SetMode(gin.ReleaseMode)
	app := boilerplate.CreateApp().
		WithLogging().
		WithDatabase(SetupDatabase).
		WithGin()
	SetupRoutes(app.Engine, "/api")

	body, _ := json.Marshal(map[string]string{
		"id": "Scheune",
		"description": "Veranstaltungsort mit Küche",
	})

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/api/subject", bytes.NewBuffer(body))
	app.Engine.ServeHTTP(w, req)

	assert.Equal(t, w.Code, 403)
}

func TestSubjectCreateWithPermission(t *testing.T) {
	gin.SetMode(gin.ReleaseMode)
	app := boilerplate.CreateApp().
		WithLogging().
		WithDatabase(SetupDatabase).
		WithGin()
	SetupRoutes(app.Engine, "/api")

	body, _ := json.Marshal(map[string]string{
		"id": "Scheune",
		"description": "Veranstaltungsort mit Küche",
	})

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/api/subject", bytes.NewBuffer(body))
	req.Header.Add("Authorization", "Bearer unit-test")
	app.Engine.ServeHTTP(w, req)

	assert.Equal(t, w.Code, 201)
}

func TestSubjectList(t *testing.T) {
	app := boilerplate.CreateApp().
		WithLogging().
		WithDatabase(SetupDatabase).
		WithGin()
	SetupRoutes(app.Engine, "/api")

	body, _ := json.Marshal(ent.Subject{ID: "Scheune", Description: "Veranstaltungsort mit Küche"})
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/api/subject", bytes.NewBuffer(body))
	req.Header.Add("Authorization", "Bearer unit-test")
	app.Engine.ServeHTTP(w, req)

	assert.Equal(t, w.Code, 201)

	bodyRes, _ := json.Marshal([]ent.Subject{{ID: "Scheune", Description: "Veranstaltungsort mit Küche"}})
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/api/subject", nil)
	app.Engine.ServeHTTP(w, req)

	assert.Equal(t, w.Code, 200)
	assert.Equal(t, w.Body.Bytes(), bodyRes)
}

func TestSubjectRetrieve(t *testing.T) {
	app := boilerplate.CreateApp().
		WithLogging().
		WithDatabase(SetupDatabase).
		WithGin()
	SetupRoutes(app.Engine, "/api")

	body, _ := json.Marshal(ent.Subject{ID: "Scheune", Description: "Veranstaltungsort mit Küche"})
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/api/subject", bytes.NewBuffer(body))
	req.Header.Add("Authorization", "Bearer unit-test")
	app.Engine.ServeHTTP(w, req)

	assert.Equal(t, w.Code, 201)

	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/api/subject/Scheune", nil)
	app.Engine.ServeHTTP(w, req)

	assert.Equal(t, w.Code, 200)
	assert.Equal(t, w.Body.Bytes(), body)
}

func TestSubjectRetrieveNotFound(t *testing.T) {
	app := boilerplate.CreateApp().
		WithLogging().
		WithDatabase(SetupDatabase).
		WithGin()
	SetupRoutes(app.Engine, "/api")

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/api/subject/Scheune", nil)
	app.Engine.ServeHTTP(w, req)

	assert.Equal(t, w.Code, 404)
}

func TestSubjectDeleteNotFound(t *testing.T) {
	app := boilerplate.CreateApp().
		WithLogging().
		WithDatabase(SetupDatabase).
		WithGin()
	SetupRoutes(app.Engine, "/api")

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("DELETE", "/api/subject/Scheune", nil)
	req.Header.Add("Authorization", "Bearer unit-test")
	app.Engine.ServeHTTP(w, req)

	assert.Equal(t, w.Code, 404)
}

func TestSubjectDeleteWithoutPermission(t *testing.T) {
	app := boilerplate.CreateApp().
		WithLogging().
		WithDatabase(SetupDatabase).
		WithGin()
	SetupRoutes(app.Engine, "/api")

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("DELETE", "/api/subject/Scheune", nil)
	app.Engine.ServeHTTP(w, req)

	assert.Equal(t, w.Code, 403)
}

func TestSubjectUpdateNotFound(t *testing.T) {
	app := boilerplate.CreateApp().
		WithLogging().
		WithDatabase(SetupDatabase).
		WithGin()
	SetupRoutes(app.Engine, "/api")

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("PUT", "/api/subject/Scheune", nil)
	req.Header.Add("Authorization", "Bearer unit-test")
	app.Engine.ServeHTTP(w, req)

	assert.Equal(t, w.Code, 404)
}

func TestSubjectUpdateWithoutPermission(t *testing.T) {
	app := boilerplate.CreateApp().
		WithLogging().
		WithDatabase(SetupDatabase).
		WithGin()
	SetupRoutes(app.Engine, "/api")

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("PUT", "/api/subject/Scheune", nil)
	app.Engine.ServeHTTP(w, req)

	assert.Equal(t, w.Code, 403)
}