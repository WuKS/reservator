package main

import (
	"codeberg.org/momar/webapp-boilerplate"
	"codeberg.org/wuks/reservator/api"
	"net/http"

	_ "github.com/mattn/go-sqlite3"
)

func main() {
	app := boilerplate.CreateApp().
		WithLogging().
		WithDatabase(api.SetupDatabase).
		WithGin().
		WithStatic("/", http.Dir("./ui/dist"), false)

	api.SetupRoutes(app.Engine, "/api")
	app.Start()
}

