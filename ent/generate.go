// +build nobuild

package ent

import (
	_ "entgo.io/ent/cmd/ent"
)

//go:generate go run entgo.io/ent/cmd/ent generate ./schema
