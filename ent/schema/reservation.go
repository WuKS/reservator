package schema

import (
	"errors"
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"github.com/google/uuid"
	"strings"
)

// Reservation holds the schema definition for the Reservation entity.
type Reservation struct {
	ent.Schema
}

// Fields of the Reservation.
func (Reservation) Fields() []ent.Field {
	return []ent.Field{
		field.UUID("id", uuid.UUID{}).
			Unique().
			Immutable().
			Comment("ID uniquely identifies the reservation using a UUID").
			StructTag(`example:"fcdebd13-d19d-4207-a863-a011d7de95f6"`),

		field.String("title").
			NotEmpty().
			Comment("Title of the reservation (e.g. event name)").
			StructTag(`example:"Loitscher Adventshof"`),

		field.Text("description").
			Optional().
			Comment("Description or other notes about the reservation").
			StructTag(`example:"Herzlich willkommen Groß und Klein zum Loitscher Adventshof! Das Programm ist auf https://de-de.facebook.com/events/262189751133021/ zu finden. Weiter erwartet Euch: Stände mit Selbstgebastelten, Weihnachtsbastelwerkstatt für Kinder, Näh und Strickwaren, Kettensägenschnitzerei und ausgesuchte leckere Speisen und Getränke. Wir freuen uns auf Euch!!!"`),

		field.String("email").
			NotEmpty().
			Validate(func(s string) error {
				if !strings.Contains(s, "@") {
					return errors.New("must be a valid email address")
				}
				return nil
			}).
			Comment("Email address of the organizer or responsible person").
			StructTag(`example:"Benjamin Otto <mail@werkundkultur.de>"`),

		field.String("contact").
			Optional().
			Comment("Contact text, for example including a phone number or radio frequencies used during an event").
			StructTag(`example:"Telefon: +49 123 4567890"`),

		field.Time("begin").
			Comment("Begin of the reservation or event").
			StructTag(`example:"2020-11-11T12:00:00Z"`),

		field.Time("end").
			Comment("End of the reservation or event").
			StructTag(`example:"2020-11-11T19:00:00Z"`),

		field.Enum("status").
			Default("request").
			Values("request", "private", "public", "declined").
			Comment("Status of the reservation, guests can only create 'request' reservations").
			StructTag(`example:"request"`),
	}
}

// Edges of the Reservation.
func (Reservation) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("subjects", Subject.Type).Ref("reservations").
			Required().
			Comment("Subjects that should be reserved in the reservation's timeframe").
			StructTag(`json:"subjects"`),
	}
}
