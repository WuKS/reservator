package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

// Subject holds the schema definition for the Subject entity.
type Subject struct {
	ent.Schema
}

// Fields of the Subject.
func (Subject) Fields() []ent.Field {
	return []ent.Field{
		field.String("id").
			Unique().
			NotEmpty().
			Comment("ID uniquely identifies the subject using a short name").
			StructTag(`example:"Scheune"`),
		field.String("description").
			Optional().
			Comment("Description of the subject").
			StructTag(`example:"Veranstaltungsraum mit Küche"`),
		field.String("icon").
			Optional().
			Comment("Icon class for the subject, see https://remixicon.com for a reference").
			StructTag(`example:"home-heart-fill"`),
		field.String("color").
			Optional().
			Comment("Color code for the subject").
			StructTag(`example:"#dd2211"`),
	}
}

// Edges of the Subject.
func (Subject) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("reservations", Reservation.Type).StructTag(`json:"reservations"`),
	}
}
