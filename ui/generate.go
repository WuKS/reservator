package ui

//go:generate openapi-generator-cli generate -i ../docs/swagger.yaml -g typescript-fetch -o ./src/api-client --additional-properties=typescriptThreePlus=true
//go:generate node -e 'https.get("https://raw.githubusercontent.com/Remix-Design/RemixIcon/v" + require("./package.json").dependencies.remixicon.replace(/^\D+/, "") + "/tags.json", (res) => res.on("data", (d) => process.stdout.write(d)))' > src/assets/remixicon-tags.json
//go:generate npm run-script build
