import {Configuration, ReservationApi, SubjectApi} from "./api-client";

const config = new Configuration({
    basePath: "/api",
});

export const api = {
    reservation: new ReservationApi(config),
    subject: new SubjectApi(config),
}
