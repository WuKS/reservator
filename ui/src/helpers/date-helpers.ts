export const dateHelpers =  {
    DAYS_BACK: 2,
    MAX_DAYS_COUNT: 17, // 2 weeks + 3 days
    WEEKDAYS: ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
    MONTHS: ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
    listOfDays: function listOfDays(start: Date): Date[] {
        const days = [];
        let current = new Date(start);
        current.setHours(0);
        current.setMinutes(0);
        current.setSeconds(0);
        for (let i = 0; i < dateHelpers.MAX_DAYS_COUNT; i++) {
            days.push(new Date(current));
            current.setDate(current.getDate() + 1);
        }
        return days;
    },
    formatDate: function formatDate(date: Date): string {
        return dateHelpers.WEEKDAYS[date.getDay()] + " " + date.getDate() + ". " + dateHelpers.MONTHS[date.getMonth()] + " " + date.getFullYear();
    },
    formatDateISO: function formatDateISO(date: Date): string {
        return date.getFullYear() + "-" + (x=>(x<10?"0":"")+x)(date.getMonth() + 1) + "-" + (x=>(x<10?"0":"")+x)(date.getDate());
    },
    isWeekend: function isWeekend(date: Date): boolean {
        return date.getDay() == 0 || date.getDay() == 6;
    },
    isToday: function isToday(date: Date): boolean {
        return dateHelpers.formatDateISO(date) == dateHelpers.formatDateISO(new Date());
    },
    isSunday: function isSunday(date: Date): boolean {
        return date.getDay() == 0;
    }
}
