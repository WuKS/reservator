export async function formatError(e: any): Promise<string> {
    if (e instanceof Response) {
        let msg = e.status + " " + e.statusText;
        try {
            msg += "\n" + await e.text();
        } catch (e) {}
        return msg;
    } else if (e instanceof Error) {
        return e.message;
    } else if (typeof e === 'string') {
        return e;
    } else {
        return JSON.stringify(e);
    }
}
